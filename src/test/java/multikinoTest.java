import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class multikinoTest {

    private WebDriver driver;
    private String baseUrl;
    private String showingId;
    private String pathLocation;

    @Test
    public void getMultikinoInfo() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "https://multikino.pl/kupbilet/7292/4/2/3764869/wybierz-miejsce";
        showingId = baseUrl.substring(39, 46);
        pathLocation = "c:\\tmp\\";

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get(baseUrl);

        driver.findElement(By.className("next-step-btn")).click();

        // this only for wait some time to populate seats, without it there is an ERROR
        String occupiedSeatsLocator = "//div[contains(@class, 'icon-user')]";
        driver.findElement(By.xpath(occupiedSeatsLocator));

        ////////////////////////////////////////////////////////////////////////////////

//        with "EKRAN" bar and size is 17 KB
//        WebElement ele = driver.findElement(By.id("cinema-seats"));

//        without "EKRAN" bar and size is 11 KB
        WebElement ele = driver.findElement(By.id("seats-container"));

        // Get entire page screenshot
        File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        BufferedImage  fullImg = ImageIO.read(screenshot);

        // Get the location of element on the page
        Point point = ele.getLocation();

        // Get width and height of the element
        int eleWidth = ele.getSize().getWidth();
        int eleHeight = ele.getSize().getHeight();

        // Crop the entire page screenshot to get only element screenshot
        BufferedImage eleScreenshot= fullImg.getSubimage(point.getX(), point.getY(),
                eleWidth, eleHeight);
        ImageIO.write(eleScreenshot, "png", screenshot);

        // Copy the element screenshot to disk
        File screenshotLocation = new File(pathLocation + showingId + ".png");
        FileUtils.copyFile(screenshot, screenshotLocation);

        /////////////////////////////////////////////////////////////////////////////////

        driver.quit();
    }
}
